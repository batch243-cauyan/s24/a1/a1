// In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
// Link the script.js file to the index.html file.
// Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
// Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
// Create a variable address with a value of an array containing details of an address.
// Destructure the array and print out a message with the full address using Template Literals.
// Create a variable animal with a value of an object data type with different animal details as its properties.
// name
// species
// weight
// measurement

// Destructure the object and print out a message with the details of the animal using Template Literals.
// Create an array of numbers.
// Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
//  Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
// Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
// Create/instantiate a new object from the class Dog and console log the object.
// Create a git repository named S24.
// Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// Add the link in Boodle.

const getCube = (num)=>{
    let cubed = Math.pow(num,3);
    console.log(`The cube of ${num} is ${cubed}`);
}
getCube(2);

let address = ['258', 'Washington Ave NW', 'California', 90011];

const [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

let animal = {
    name: 'Lolong',
    species: 'saltwater crocodile',
    weight: '1075 kgs',
    measurement: `20 ft 3 in`
}

let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed ${weight} with a measurement of ${measurement}.`);

let nums = [1,2,3,4,5];

nums.forEach(num=>{
    console.log(num);
})


let reduceNumber = nums.reduce((sum, num)=> sum + num);
console.log(reduceNumber);


class Dog {
    constructor (name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let dog1 = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(dog1);
